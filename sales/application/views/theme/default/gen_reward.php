<?php
$sale_roup = array(
101 => "กทม. ปริมณฑล",
102 => "ภาคกลาง",
201 => "ภาคเหนือบน",
202 => "ภาคเหนือล่าง 1",
301 => "ภาคตะวันออก 1",
302 => "ภาคตะวันออก 2",
401 => "ภาคอีสานบน",
501 => "ภาคตะวันตก",
601 => "ภาคใต้"
);

$date_issue = array('วาระ', 
	'12/2014', 
	'01/2015',
	'02/2015',
	'03/2015',
	'04/2015',
	'05/2015',
	'06/2015',
	'07/2015',
	'08/2015',
	'09/2015',
	'10/2015',
	'11/2015',
	'12/2015',
	'01/2016'
);
$report_issue = $this->input->get('report_issue');
?>

<form action="<?php echo ($is_rewrite) ? base_url('/reward/mailing/'):site_url('/reward/mailing/');?>" method="post" target="_blank">
<div style="page-break-after: always;">
<table width="100%" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="bottom"><h1 style="margin: 0; padding: 0;">Sotus Reward <?php echo $date_issue[$report_issue]; ?></h1></td>
    <td style="width: 142px;" rowspan="2" align="right"><img src="<?php echo base_url('images'); ?>/logo-20th_142x150.png" border="0"></td>
  </tr>
</table>
<br>
<div class="text-right hidden-inline-print"><a href="<?php echo base_url(index_page().$this->uri->uri_string()); ?>/?report_issue=<?php echo $this->input->get('report_issue'); ?>&sort=<?php echo ($this->input->get('sort') == 0) ? 1:0; ?>">เรียงตามรหัส</a>
</div>
		<table border="0" cellpadding="4" cellspacing="0" width="98%" align="center" style="border: 1px solid #000;" class="table table-bordered">
		<thead bgcolor="#CCFFCC">
		  <tr>
		    <th rowspan="2" class="text-center hidden-inline-print">#</th>
		    <th rowspan="2" class="text-center">No.</th>
		    <th rowspan="2" class="text-center">เขต</th>
		    <th rowspan="2" class="text-center">รหัส</th>
		    <th rowspan="2" class="text-center">กลุ่มสินค้า</th>
		    <th colspan="2" class="text-center">ยอดซื้อ</th>
		    <th colspan="2" class="text-center">คูปองจับรางวัลครั้งที่ <?php echo ($report_issue > 7) ? '2':'1'; ?></th>
		    <th rowspan="2" class="text-center">เกษตรกร</th>
		    <th rowspan="2" class="text-center">คะแนนสะสม</th>
	      </tr>
		  <tr>
		    <th class="text-center">เดือนนี้</th>
		    <th class="text-center">สะสม</th>
		    <th class="text-center">เดือนนี้</th>
		    <th class="text-center">สะสม</th>
	      </tr>
		</thead>
		<?php
		$r = 1;
		$pi = 1;
		$sum_net_value = 0;
		$sum_net_coupon = 0;
		$sum_co_coupon = 0;
		$sum_point = 0;
		$sum_net_group = 0;
		$sum_net_value_issue = 0;
		$sum_net_group_issue = 0;
		$sum_net_coupon_issue = 0;
		foreach ($gen_reward as $row)
		{
			

			$sum_net_value = $sum_net_value + $row->net_value;
			$sum_net_coupon = $sum_net_coupon + $row->net_coupon;
			$sum_co_coupon = $sum_co_coupon + $row->co_coupon;
			$sum_point = $sum_point + $row->net_point;

			$net_group = $row->net_a + $row->net_b;
			$sum_net_value_issue = $sum_net_value_issue + $row->net_value_issue;
			$sum_net_group = $sum_net_group + $net_group;

			
			$net_group_issue = $row->net_a_issue + $row->net_b_issue;
			$sum_net_group_issue = $sum_net_group_issue + $net_group_issue;

			$sum_net_coupon_issue = $sum_net_coupon_issue + $row->net_coupon_issue;

			?>
		  <tr>
		    <td class="text-center hidden-inline-print"><input type="checkbox" name="pkid[<?php echo $row->pk; ?>]" <?php echo ($row->flag == 1) ? 'checked':'/'; ?>></td>
		    <td class="text-center"><?php echo $r; ?></td>
		    <td class="text-left" nowrap="nowrap"><?php echo $row->region_group; ?> <?php echo $sale_roup[$row->region_group]; ?></td>
		    <td class="text-center"><?php echo $row->sold_to; ?></td>
		    <td class="text-left">สินค้ากลุ่ม A</td>
		    <td class="text-right"><?php echo number_format($row->net_a_issue, 2, ".", ","); ?></td>
		    <td class="text-right"><?php echo number_format($row->net_a, 2, ".", ","); ?></td>
		    <td class="text-right"><?php echo number_format($row->coupon_a_issue, 0, ".", ","); ?></td>
		    <td class="text-right"><?php echo number_format($row->coupon_a, 0, ".", ","); ?></td>
		    <td class="text-right">&nbsp;</td>
		    <td class="text-right"><?php echo number_format($row->point_a, 0, ".", ","); ?></td>
	      </tr>
		  <tr>
		    <td class="text-center hidden-inline-print">&nbsp;</td>
		    <td class="text-center">&nbsp;</td>
		    <td colspan="2" class="text-left"><?php echo $row->prefix; ?> <?php echo $row->party_name; ?></td>
		    <td class="text-left">สินค้ากลุ่ม B</td>
		    <td class="text-right"><?php echo number_format($row->net_b_issue, 2, ".", ","); ?></td>
		    <td class="text-right"><?php echo number_format($row->net_b, 2, ".", ","); ?></td>
		    <td class="text-right"><?php echo number_format($row->coupon_b_issue, 0, ".", ","); ?></td>
		    <td class="text-right"><?php echo number_format($row->coupon_b, 0, ".", ","); ?></td>
		    <td class="text-right">&nbsp;</td>
		    <td class="text-right"><?php echo number_format($row->point_b, 0, ".", ","); ?></td>
	      </tr>
		  <tr style="background-color: #FF0;">
		    <td class="text-center hidden-inline-print">&nbsp;</td>
		    <td class="text-center">&nbsp;</td>
		    <td class="text-center">&nbsp;</td>
		    <td class="text-center">&nbsp;</td>
		    <td class="text-right"><?php echo number_format($row->net_value_issue, 2, ".", ","); ?></td>
		    <td class="text-right"><?php echo number_format($net_group_issue, 2, ".", ","); ?></td>
		    <td class="text-right"><?php echo number_format($net_group, 2, ".", ","); ?></td>
		    <td class="text-right"><?php echo number_format($row->net_coupon_issue, 0, ".", ","); ?></td>
		    <td class="text-right"><?php echo $row->net_coupon; ?></td>
		    <td class="text-right"><?php echo $row->co_coupon; ?></td>
		    <td class="text-right"><?php echo $row->net_point; ?></td>
	      </tr>
			<?php
			if ($r%7 == 0) {
				?>
				</table>
<br>
<table width="100%">
<tr>
<td><?php echo date("d/m/").(date("Y") + 543)." ". date("H:i:s"); ?></td>
<td align="right">หน้า <?php echo $pi; ?></td>
</table>
				</div>
				<br>
				<div style="page-break-after: always;">
<table width="100%" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="bottom"><h1 style="margin: 0; padding: 0;">Sotus Reward</h1></td>
    <td style="width: 142px;" rowspan="2" align="right"><img src="<?php echo base_url('images'); ?>/logo-20th_142x150.png" border="0"></td>
  </tr>
</table>
<br>
<table border="0" cellpadding="4" cellspacing="0" width="98%" align="center" style="border: 1px solid #000;" class="table table-bordered">
		<thead bgcolor="#CCFFCC">
		  <tr>
		    <th rowspan="2" class="text-center hidden-inline-print">#</th>
		    <th rowspan="2" class="text-center">No.</th>
		    <th rowspan="2" class="text-center">เขต</th>
		    <th rowspan="2" class="text-center">รหัส</th>
		    <th rowspan="2" class="text-center">กลุ่มสินค้า</th>
		    <th colspan="2" class="text-center">ยอดซื้อ</th>
		    <th colspan="2" class="text-center">คูปองจับรางวัลครั้งที่ <?php echo ($report_issue > 7) ? '2':'1'; ?></th>
		    <th rowspan="2" class="text-center">เกษตรกร</th>
		    <th rowspan="2" class="text-center">คะแนนสะสม</th>
	      </tr>
		  <tr>
		    <th class="text-center">เดือนนี้</th>
		    <th class="text-center">สะสม</th>
		    <th class="text-center">เดือนนี้</th>
		    <th class="text-center">สะสม</th>
	      </tr>
		</thead>
				<?php
				$pi++;
			}
			$r++;
		}
		?>
		  <tr style="background-color: #FC0;">
		    <td class="text-center hidden-inline-print">&nbsp;</td>
		    <td colspan="3" class="text-center">&nbsp;</td>
            <td class="text-right"><?php echo number_format($sum_net_value_issue, 2, ".", ","); ?></td>
		    <td class="text-right"><?php echo number_format($sum_net_group_issue, 2, ".", ","); ?></td>
		    <td class="text-right"><?php echo number_format($sum_net_group, 2, ".", ","); ?></td>
		    <td class="text-right"><?php echo number_format($sum_net_coupon_issue, 0, ".", ","); ?></td>
		    <td class="text-right"><?php echo number_format($sum_net_coupon, 0, ".", ","); ?></td>
		    <td class="text-right"><?php echo number_format($sum_co_coupon, 0, ".", ","); ?></td>
		    <td class="text-right"><?php echo number_format($sum_point, 0, ".", ","); ?></td>
	      </tr>
		</table>
		<br>
<table width="100%">
<tr>
<td><?php echo date("d/m/").(date("Y") + 543)." ". date("H:i:s"); ?></td>
<td align="right">หน้า <?php echo $pi; ?></td>
</table>
</div>
<div class=" hidden-inline-print">
<p>
<h2>ต้นจดหมาย</h2>
<textarea name="header_p1" id="mail_message" style="width:98%; height:100px; margin: 10px auto;"><?php echo $mail_text['header_1']; ?></textarea>
<textarea name="header_p2" id="mail_message" style="width:98%; height:100px; margin: 10px auto;"><?php echo $mail_text['header_2']; ?></textarea>
<textarea name="header_p3" id="mail_message" style="width:98%; height:100px; margin: 10px auto;"><?php echo $mail_text['header_3']; ?></textarea>
</p>
<p>
<h2>ท้ายจดหมาย</h2>
<textarea name="footer_p1" id="mail_footer" style="width:98%; height:100px; margin: 10px auto;"><?php echo $mail_text['footer_1']; ?></textarea>
<textarea name="footer_p2" id="mail_footer" style="width:98%; height:100px; margin: 10px auto;"><?php echo $mail_text['footer_2']; ?></textarea>
<textarea name="footer_p3" id="mail_footer" style="width:98%; height:100px; margin: 10px auto;"><?php echo $mail_text['footer_3']; ?></textarea>
</p>
<br>
<div style="text-align:center"><input type="submit" value="สร้างจดหมาย"></div>
</div>

</form>