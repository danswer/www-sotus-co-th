<?php
class sotus_invoice extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }

	public function document_list()
	{
		$sql = "SELECT * 
		FROM  `invoice_doc`";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function customers() {
		$query = $this->db->query("SELECT `customer`.credit_group, 
		`customer`.prefix_name, 
		`customer`.customer_name, 
		`customer`.street_addr, 
		`customer`.street4, 
		`customer`.street5, 
		`customer`.city_name, 
		`customer`.region_name, 
		`customer`.region_group, 
		`customer`.postal_code,
		`reward_cust`.`flag` 
		FROM `customer`, `reward_cust` WHERE `customer`.customer = `customer`.credit_group AND `reward_cust`.`party_code` = `customer`.customer
		GROUP BY `customer`.credit_group");
		return $query->result();
	}
/*
	public function get_invoice_reward() {
		$sql = "SELECT
		invoice.billing_doc,
		invoice.billing_date,
		customer.credit_group,
		customer.customer,
		customer.prefix_name,
		customer.customer_name,
		material.reward_group,
		invoice.material_code,
		invoice.material_label,
		invoice.itca,
		invoice.billing_quantity,
		invoice.billing_su,
		invoice.net_value
		FROM
		customer
		INNER JOIN reward_cust ON customer.customer = reward_cust.party_code
		INNER JOIN invoice ON customer.customer = invoice.sold_to
		INNER JOIN material ON invoice.material_code = material.material
		WHERE
		reward_cust.flag = 1";
		$query = $this->db->query($sql);
		return $query->result();
	}
*/
	public function get_credit_group() {
		$sql = "SELECT
		customer.credit_group,
		customer.customer,
		customer.prefix_name,
		customer.customer_name,
		reward_cust.flag
		FROM
		customer
		INNER JOIN reward_cust ON customer.customer = reward_cust.party_code
		WHERE
		reward_cust.flag = 1 AND
		customer.customer = customer.credit_group
		GROUP BY
		customer.credit_group";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_invoice_reward() {
		$rs = $this->get_credit_group();
		foreach ($rs as $field)
		{
			$sql = "SELECT
			invoice.billing_doc,
			invoice.billing_date,
			customer.credit_group,
			customer.customer,
			customer.prefix_name,
			customer.customer_name,
			material.reward_group,
			invoice.material_code,
			invoice.material_label,
			invoice.itca,
			invoice.billing_quantity,
			invoice.billing_su,
			invoice.net_value,
			SUM(net_value) AS net_value
			FROM
			invoice
			INNER JOIN material ON invoice.material_code = material.material
			INNER JOIN customer ON invoice.sold_to = customer.customer
			WHERE
			invoice.billing_date BETWEEN '2014-12-01' AND '2014-12-31' AND
			invoice.sold_to = customer.customer AND
			customer.credit_group = ".$field->credit_group."
			GROUP BY
			customer.credit_group";
			$query = $this->db->query($sql);
			return $query->result();
		}

	}
}

/* End of file sotus_invoice.php */
/* Location: ./application/models/sotus_invoice.php */