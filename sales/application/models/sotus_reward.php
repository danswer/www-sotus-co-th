<?php
class sotus_reward extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }

	public function hello()
	{	
		echo "Hello!";
	}

	public function get_credit_group()
	{
		$sql = "SELECT  `pk`, `customer` ,  `credit_group`,  `prefix_name` ,  `customer_name` ,  `city_name` ,  `region_name`, `region_group`, 
		(SELECT `group_prefix` FROM `credit_group` WHERE `group_code` = `credit_group`) as 'group_prefix', 
		(SELECT `group_name` FROM `credit_group` WHERE `group_code` = `credit_group`) as 'group_name',
		(SELECT  `flag` FROM  `reward_cust` WHERE `party_code` LIKE `customer`) as 'join_reward'
		FROM  `customer` 
		WHERE `customer` = `credit_group`
		ORDER BY `region_group`, `credit_group`";
		
		$query = $this->db->query($sql);

		return $query->result();
	}

	public function num_customer_ingroup($credit_group)
	{
		$query = $this->db->select('customer');
		$query = $this->db->where('credit_group', $credit_group);
		$query = $this->db->get('customer');

		return $query->num_rows();
	}

	public function get_customer_ingroup($credit_group)
	{
		/*
		$query = $this->db->select('customer');		
		$query = $this->db->where('credit_group', $credit_group);
		$query = $this->db->order_by('customer');
		$query = $this->db->get('customer');
		*/
		$sql = "SELECT *,
		(SELECT  `flag` FROM  `reward_cust` WHERE `party_code` LIKE `customer`) as 'join_reward' FROM `customer` WHERE credit_group = ".$credit_group;
		$query = $this->db->query($sql);

		return $query->result();
	}

	public function get_customers()
	{
		$sql = "SELECT  `customer` ,  `credit_group`,  `prefix_name` ,  `customer_name` ,  `city_name` ,  `region_name` 
		FROM  `customer` 
		GROUP BY  `customer` 
		ORDER BY  `customer_name`";
		
		$query = $this->db->query($sql);

		return $query->result();
	}

	// Invoice

	public function get_reward_inv()
	{
		$report_issue = intval($this->input->get('report_issue'));

		$this->db->select_max('issue');
		$query = $this->db->get('reward_inv');
		$num = $query->num_rows();

		$sql = "SELECT  `pk`, `billing_doc` ,  `billing_date` ,  `credit_group` ,  (SELECT `customer_name` FROM customer WHERE `customer`= `reward_inv`.`credit_group`) AS 'c_name', `sold_to` ,  `party_name` ,  `material_code` ,  `material_label` ,  `itca` ,  `billing_quantity` ,  `billing_su` ,  `net_value` ,  `reward_group` ,  `reward_flag` ,  `issue` 
FROM  `reward_inv` WHERE issue = ".(($report_issue > 0) ? $report_issue:$num  )." ORDER BY `credit_group`, `billing_date`";

		$query = $this->db->query($sql);

		$rs = $query->result();
		return $rs;
	}

	public function insert_reward_inv() {
		header('Content-Type: text/html; charset=utf-8');
		$report_issue = $this->input->post('report_issue');

		$this->db->where('issue', $report_issue);
		$num_rows = $this->db->count_all_results('invoice');

		if ($num_rows == 0) {
			?>
			<div>Data not found</div>
			<a href="<?php echo base_url(index_page().'reward/invoice'); ?>">Return</a>
			<?php
			die();
		}

		$sql = "SELECT `pk`, `billing_doc`, `billing_date`, (SELECT `credit_group` 
FROM `customer` WHERE `customer`.`customer`=`invoice`.`sold_to`) as 'credit_group', `sold_to`, `party_name`, `material_code`, `material_label`, `itca`, `billing_quantity`, `billing_su`, `net_value`, (SELECT `material`.`reward_group` FROM  `material` WHERE `material`.`material` LIKE  `invoice`.`material_code`) as 'reward_group', (SELECT  `reward_cust`.`flag` FROM  `reward_cust` WHERE  `reward_cust`.`party_code` LIKE  `invoice`.`sold_to`) as 'reward_flag', issue FROM `invoice` WHERE `issue` = ". $report_issue;

		$query = $this->db->query($sql);
		$rs = $query->result();

		//$this->db->query("TRUNCATE TABLE `reward_inv`");
		$this->db->query("DELETE FROM `reward_inv` WHERE `issue` = ". $report_issue);

		$n = 0;
		foreach ($rs as $row)
		{
			//billing_doc	billing_date	credit_group	sold_to	party_name	material_code	material_label	itca	billing_quantity	billing_su	net_value	reward_group
			if ($row->credit_group) {
				$n = $n+1;
				$sql_inv = "INSERT INTO reward_inv (billing_doc, billing_date, credit_group, sold_to, party_name, material_code, material_label, itca, billing_quantity, billing_su, net_value, reward_group, reward_flag, issue) VALUES('".$row->billing_doc."', '".$row->billing_date."', '".$row->credit_group."', '".$row->sold_to."', '". $row->party_name ."', '". $row->material_code ."', '". $row->material_label ."', '". $row->itca ."', '". $row->billing_quantity ."', '". $row->billing_su ."', '". $row->net_value ."', '". $row->reward_group ."', '".$row->reward_flag."', ".$row->issue .")";

				//echo $n.") ".$sql_inv.";<br>";
				//echo $n."|".$row->billing_doc."|" .$row->billing_date. "|" .$row->credit_group."|". $row->sold_to ."|". $row->party_name ."|". $row->material_code ."|". $row->material_label ."|". $row->itca ."|". $row->billing_quantity ."|". $row->billing_su ."|". $row->net_value ."|". $row->reward_group ."|".$row->reward_flag."|".$report_issue."<br>";

				$this->db->query($sql_inv);
				
			}
			
		}

		return $n;
	}

	public function insert_reward_val() {
		header('Content-Type: text/html; charset=utf-8');

		$report_issue = $this->input->post('report_issue');

		$this->db->where('issue', $report_issue);
		$num_rows = $this->db->count_all_results('reward_inv');

		if ($num_rows == 0) {
			?>
			<div>Data not found</div>
			<a href="<?php echo base_url(index_page().'reward/view'); ?>">Return</a>
			<?php
			die();
		}

		//$this->db->query("TRUNCATE TABLE `reward_value`");
		
		$this->db->query("DELETE FROM `reward_value` WHERE `issue` = ". $report_issue);
		
		$sql = "SELECT `customer`.credit_group, 
`credit_group`.group_prefix,
`credit_group`.group_name,
		`customer`.street_addr, 
		`customer`.street4, 
		`customer`.street5, 
		`customer`.city_name, 
		`customer`.region_name, 
		`customer`.region_group, 
		`customer`.postal_code,
		`reward_cust`.`flag` 
		FROM `customer`, `reward_cust`, `credit_group` WHERE `customer`.customer = `customer`.credit_group AND `reward_cust`.`party_code` = `customer`.customer AND
`credit_group`.group_code = `customer`.credit_group
		GROUP BY `customer`.credit_group";

		$query = $this->db->query($sql);
		$rs = $query->result();
		foreach ($rs as $row)
		{
			if ($row->flag == 1) {
				$sql = "SELECT SUM(net_value) as 'net_value', (

				SELECT SUM( net_value ) 
				FROM reward_inv
				WHERE credit_group = '".$row->credit_group."'
				AND reward_group =  'A'
				) AS  'net_a', (

				SELECT SUM( net_value ) 
				FROM reward_inv
				WHERE credit_group = '".$row->credit_group."'
				AND reward_group =  'B'
				) AS  'net_b'

				FROM reward_inv WHERE credit_group = '".$row->credit_group."'";
				//echo $sql."<br>";
				$query = $this->db->query($sql);
				$value = $query->row();

				$net_value = $value->net_value;

				$coupon_a = $value->net_a / 100000;
				$coupon_b = $value->net_b / 200000;

				if ($coupon_a < 2) $coupon_a = 0;
				if ($coupon_b < 1) $coupon_b = 0;

				$ax = $coupon_a - intval($coupon_a);
				if ($ax >= 0.5) {
					$coupon_a = intval($coupon_a) + 1;
				} else {
					$coupon_a = intval($coupon_a);
				}

				$coupon_b = round($coupon_b);

				$net_coupon = ceil(($coupon_a + $coupon_b));

				$point_a = ($value->net_a / 200000 );
				$point_b = ($value->net_b / 200000 );

				if ($point_a < 1) $point_a = 0;
				if ($point_b < 1) $point_b = 0;

				$point_a = round($point_a) * 50;
				$point_b = round($point_b) * 20;
				
				$net_point = $point_a + $point_b;


				// Monthly
				$sql_issue = "SELECT SUM(net_value) as 'net_value', (

				SELECT SUM( net_value ) 
				FROM reward_inv
				WHERE credit_group = '".$row->credit_group."'
				AND reward_group =  'A' AND issue = ".$report_issue."
				) AS  'net_a_issue', (

				SELECT SUM( net_value ) 
				FROM reward_inv
				WHERE credit_group = '".$row->credit_group."'
				AND reward_group =  'B' AND issue = ".$report_issue."
				) AS  'net_b_issue'

				FROM reward_inv WHERE issue = ".$report_issue." AND credit_group = '".$row->credit_group."'";
				
				$query_issue = $this->db->query($sql_issue);
				$value_issue = $query_issue->row();

				$net_value_issue = $value_issue->net_value;

				$co_coupon = floor( ($value_issue->net_value / 40000) );

				$coupon_a_issue = $value_issue->net_a_issue / 100000;
				$coupon_b_issue = $value_issue->net_b_issue / 200000;

				if ($coupon_a_issue < 2) $coupon_a_issue = 0;
				if ($coupon_b_issue < 1) $coupon_b_issue = 0;

				$ax_issue = $coupon_a_issue - intval($coupon_a_issue);
				if ($ax_issue >= 0.5) {
					$coupon_a_issue = intval($coupon_a_issue) + 1;
				} else {
					$coupon_a_issue = intval($coupon_a_issue);
				}

				$coupon_b_issue = round($coupon_b_issue);

				$net_coupon_issue = ceil(($coupon_a_issue + $coupon_b_issue));

				//print_r($value);
				//street_addr, street4, street5, city_name
				//contact_addr	province postal_code
				
				$sql = "INSERT INTO reward_value (sold_to, prefix, party_name, contact_addr, city, province, 
				postal_code, coupon_a, coupon_b, point_a, point_b, net_coupon, net_point, co_coupon, net_value, net_a, net_b, region_group, issue, net_value_issue, net_a_issue, net_b_issue,
				coupon_a_issue, coupon_b_issue, net_coupon_issue) VALUES ('".$row->credit_group."', '".$row->group_prefix."', '".$row->group_name."', 
				'".$row->street_addr.(($row->street4) ? " ".$row->street4:"").(($row->street5) ? ' '.$row->street5:'')."', 
				'".$row->city_name."',
				'". $row->region_name ."', 
				'". $row->postal_code ."',
				'". $coupon_a ."', '". $coupon_b ."', '". $point_a ."', '". $point_b ."', '". $net_coupon ."', '". $net_point ."', '". $co_coupon ."', '".$value->net_value."', '".$value->net_a."', '".$value->net_b."', '". $row->region_group ."', ".$report_issue.", '".$net_value_issue."', '".$value_issue->net_a_issue."', '".$value_issue->net_b_issue."',
				'".$coupon_a_issue."',
				'".$coupon_b_issue."',
				'".$net_coupon_issue."')";
				//echo $sql."<br>";
				//$print_test .= $sql."<br>";
				$this->db->query($sql);
			} // Flag == 1
		}
		//return $print_test;
		
		echo "Insert reward value issue #".$this->input->post('report_issue')."<br>";
	}

	public function gen_invoice_reward()
	{
		$query = $this->db->query("SELECT `pk`, `billing_doc`, `billing_date`, (SELECT `credit_group` 
FROM `customer` WHERE `customer`.`customer`=`invoice`.`sold_to`) as 'credit_group', `sold_to`, `party_name`, `material_code`, `material_label`, `itca`, `billing_quantity`, `billing_su`, `net_value`, (SELECT `material`.`reward_group` FROM  `material` WHERE `material`.`material` LIKE  `invoice`.`material_code`) as 'reward_group', (SELECT  `reward_cust`.`flag` FROM  `reward_cust` WHERE  `reward_cust`.`party_code` LIKE  `invoice`.`sold_to`) as 'reward_flag' FROM `invoice`");
		$rs = $query->result();
		//pk	billing_doc	billing_date	sold_to	partner_code	employee_name	party_name	material_code	material_label	itca	billing_quantity	billing_su	net_value

		$this->db->query("TRUNCATE TABLE `reward_inv`");
		foreach ($rs as $row)
		{
			//billing_doc	billing_date	credit_group	sold_to	party_name	material_code	material_label	itca	billing_quantity	billing_su	net_value	reward_group
			if ($row->credit_group) {
				$this->db->query("INSERT INTO reward_inv (billing_doc, billing_date, credit_group, sold_to, party_name, material_code, material_label, itca, billing_quantity, billing_su, net_value, reward_group, reward_flag) VALUES('".$row->billing_doc."', '".$row->billing_date."', '".$row->credit_group."', '".$row->sold_to."', '". $row->party_name ."', '". $row->material_code ."', '". $row->material_label ."', '". $row->itca ."', '". $row->billing_quantity ."', '". $row->billing_su ."', '". $row->net_value ."', '". $row->reward_group ."', '".$row->reward_flag."')");
			}

			/* 
			$row->billing_doc
			$row->billing_date
			$row->credit_group
			$row->sold_to
			$row->party_name
			$row->material_code
			$row->material_label
			$row->itca
			$row->billing_quantity
			$row->billing_su
			$row->net_value
			$row->reward_group 
			*/
		}

		return $rs;

	}
/*
	public function insert_reward_value() 
	{
		//$query = $this->db->query("SELECT reward_inv.credit_group, customer.prefix_name, customer.customer_name, customer.street_addr, customer.region_name, customer.region_group FROM reward_inv, customer WHERE reward_inv.credit_group = customer.customer AND customer.customer = customer.credit_group GROUP BY reward_inv.credit_group");

		$this->db->query("TRUNCATE TABLE `reward_value`");

		//street_addr	street4	street5	city_name	region_name
		$query = $this->db->query("SELECT `customer`.credit_group, 
`credit_group`.group_prefix,
`credit_group`.group_name,
		`customer`.street_addr, 
		`customer`.street4, 
		`customer`.street5, 
		`customer`.city_name, 
		`customer`.region_name, 
		`customer`.region_group, 
		`customer`.postal_code,
		`reward_cust`.`flag` 
		FROM `customer`, `reward_cust`, `credit_group` WHERE `customer`.customer = `customer`.credit_group AND `reward_cust`.`party_code` = `customer`.customer AND
`credit_group`.group_code = `customer`.credit_group
		GROUP BY `customer`.credit_group");
		$rs = $query->result();
		foreach ($rs as $row)
		{
			if ($row->flag == 1) {
				$sql = "SELECT SUM(net_value) as 'net_value', (

				SELECT SUM( net_value ) 
				FROM reward_inv
				WHERE credit_group = '".$row->credit_group."'
				AND reward_group =  'A'
				) AS  'net_a', (

				SELECT SUM( net_value ) 
				FROM reward_inv
				WHERE credit_group = '".$row->credit_group."'
				AND reward_group =  'B'
				) AS  'net_b'

				FROM reward_inv WHERE credit_group = '".$row->credit_group."'";
				//echo $sql."<br>";
				$query = $this->db->query($sql);
				$value = $query->row();

				$net_value = $value->net_value;

				$coupon_a = $value->net_a / 100000;
				$coupon_b = $value->net_b / 200000;

				if ($coupon_a < 2) $coupon_a = 0;
				if ($coupon_b < 1) $coupon_b = 0;

				$ax = $coupon_a - intval($coupon_a);
				if ($ax >= 0.5) {
					$coupon_a = intval($coupon_a) + 1;
				} else {
					$coupon_a = intval($coupon_a);
				}

				$coupon_b = round($coupon_b);

				$net_coupon = ceil(($coupon_a + $coupon_b));

				$point_a = ($value->net_a / 200000 );
				$point_b = ($value->net_b / 200000 );

				if ($point_a < 1) $point_a = 0;
				if ($point_b < 1) $point_b = 0;

				$point_a = round($point_a) * 50;
				$point_b = round($point_b) * 20;
				
				$net_point = $point_a + $point_b;

				$co_coupon = floor( ($value->net_value / 40000) );

				//print_r($value);
				//street_addr, street4, street5, city_name
				//contact_addr	province postal_code

				$sql = "INSERT INTO reward_value (sold_to, prefix, party_name, contact_addr, city, province, 
				postal_code, coupon_a, coupon_b, point_a, point_b, net_coupon, net_point, co_coupon, net_value, net_a, net_b, region_group) VALUES ('".$row->credit_group."', '".$row->group_prefix."', '".$row->group_name."', 
				'".$row->street_addr.(($row->street4) ? " ".$row->street4:"").(($row->street5) ? ' '.$row->street5:'')."', 
				'".$row->city_name."',
				'". $row->region_name ."', 
				'". $row->postal_code ."',
				'". $coupon_a ."', '". $coupon_b ."', '". $point_a ."', '". $point_b ."', '". $net_coupon ."', '". $net_point ."', '". $co_coupon ."', '".$value->net_value."', '".$value->net_a."', '".$value->net_b."', '". $row->region_group ."')";
				//echo $sql."<br>";
				//$print_test .= $sql."<br>";
				$this->db->query($sql);
			} // Flag == 1
		}
		//return $print_test;
	}
	*/

	public function get_reward_value($order = 0) {
		$report_issue = $this->input->get('report_issue');
		if ($order == 1) {
			$query = $this->db->query("SELECT `reward_value`.`pk`, 
				`reward_value`.`sold_to`, 
				`reward_value`.`prefix`, 
				`reward_value`.`party_name`, 
				`reward_value`.`net_value`, 
				`reward_value`.`net_a`, 
				`reward_value`.`net_b`, 
				`reward_value`.`net_coupon`, 
				`reward_value`.`net_point`, 
				`reward_value`.`co_coupon`, 
				`reward_value`.`coupon_a`, 
				`reward_value`.`coupon_b`, 
				`reward_value`.`point_a`, 
				`reward_value`.`point_b`, 
				`reward_value`.`region_group`, 
				`reward_cust`.`flag`, 
				`reward_value`.`net_value_issue`, 
				`reward_value`.`net_a_issue`, 
				`reward_value`.`net_b_issue`, 
				`reward_value`.`coupon_a_issue`, 
				`reward_value`.`coupon_b_issue`, 
				`reward_value`.`net_coupon_issue`
			FROM `reward_value`, `reward_cust` 
			WHERE `reward_cust`.`party_code` = `reward_value`.`sold_to` AND `reward_cust`.`flag` = 1 
			AND `reward_value`.`issue` = ".$report_issue." 
			ORDER BY `reward_value`.`sold_to` ASC");
		} else {
			$query = $this->db->query("SELECT `reward_value`.`pk`, 
				`reward_value`.`sold_to`, 
				`reward_value`.`prefix`, 
				`reward_value`.`party_name`, 
				`reward_value`.`net_value`, 
				`reward_value`.`net_a`, 
				`reward_value`.`net_b`, 
				`reward_value`.`net_coupon`, 
				`reward_value`.`net_point`, 
				`reward_value`.`co_coupon`, 
				`reward_value`.`coupon_a`, 
				`reward_value`.`coupon_b`, 
				`reward_value`.`point_a`, 
				`reward_value`.`point_b`, 
				`reward_value`.`region_group`, 
				`reward_cust`.`flag`, 
				`reward_value`.`net_value_issue`, 
				`reward_value`.`net_a_issue`, 
				`reward_value`.`net_b_issue`, 
				`reward_value`.`coupon_a_issue`, 
				`reward_value`.`coupon_b_issue`, 
				`reward_value`.`net_coupon_issue`
			FROM `reward_value`, `reward_cust` 
			WHERE `reward_cust`.`party_code` = `reward_value`.`sold_to` AND `reward_cust`.`flag` = 1  
			AND `reward_value`.`issue` = ".$report_issue." 
			ORDER BY `reward_value`.`region_group` ASC, 
			`reward_value`.`party_name` ASC");
		}
		$rs = $query->result();
		return $rs;
	}

	public function get_material_fields() {
		$query = $this->db->list_fields('material');
		return $query;
	}

	public function get_material() {
		$query = $this->db->get('material');
		$rs = $query->result();
		return $rs;
	}

	public function mail_text_insert() {
		$header_p1 = $this->input->post('header_p1');
		$header_p2 = $this->input->post('header_p2');
		$header_p3 = $this->input->post('header_p3');

		$footer_p1 = $this->input->post('footer_p1');
		$footer_p2 = $this->input->post('footer_p2');
		$footer_p3 = $this->input->post('footer_p3');

		$query = $this->db->query("TRUNCATE TABLE mail_text");

		$sql = "INSERT INTO  `sotus_sales`.`mail_text` (
		`header_1` ,
		`header_2` ,
		`header_3` ,
		`footer_1` ,
		`footer_2` ,
		`footer_3`
		)
		VALUES (
			'".$header_p1."' , '".$header_p2."' , '".$header_p3."' , '".$footer_p1."' , '".$footer_p2."' , '".$footer_p3."'
		)";
		$query = $this->db->query($sql);
	}

	public function mail_text() {
		$this->db->from('mail_text');
		$this->db->limit(1);
		$query = $this->db->get();
		foreach($query->result_array() as $row) {
			$rs = array(
				'header_1' => $row['header_1'],
				'header_2' => $row['header_2'],
				'header_3' => $row['header_3'],
				'footer_1' => $row['footer_1'],
				'footer_2' => $row['footer_2'],
				'footer_3' => $row['footer_3'],
			);
		}
		return $rs;
	}

}

/* End of file sotus_reward.php */
/* Location: ./application/models/sotus_reward.php */