<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reward extends CI_Controller {

	/**
	 * Reward Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/reward
	 *	- or -  
	 * 		http://example.com/index.php/reward/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/reward/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('member_login');
		$member_info = $this->member_login->check();
		if ($member_info == -1) {
			$data['reUrl'] = base_url(index_page().$this->uri->uri_string());
			die('Access denied / <a href="'. base_url(index_page().'login/form/?reurl='.$this->uri->uri_string()).'">Return page</a>');

		}

		//die("Hello, ".$member_info[1]);
		if ($this->agent->is_mobile('iphone'))
		{
			$this->data['device_agent'] = $this->agent->mobile();
		}
		else if ($this->agent->is_mobile())
		{
			$this->data['device_agent'] = $this->agent->mobile();
		}
		else
		{
			$this->data['device_agent'] = $this->agent->browser().' '.$this->agent->version();
		}

		$this->data['is_rewrite'] = in_array('mod_rewrite', apache_get_modules());

	}

	public function login_form($reUrl) {

		$data['page_title'] = "Login";
		$data['page_desc'] = "Member login";
		$data['page_keywords'] = "";
		$data['reUrl'] = $reUrl;

		$this->load->view('theme/default/login', $data);

	}

	public function index()
	{
		$this->load->model('sotus_reward');
		$this->load->view('welcome_message');
	}

	public function hello()
	{
		$this->load->model('sotus_reward');
		$this->sotus_reward->hello();
	}

	public function cust()
	{
		$this->load->model('sotus_reward');

		$data['page_title'] = "Customer generator";
		$data['page_desc'] = "Customer generator";
		$data['page_keywords'] = "";
		$data['device_agent'] = $this->data['device_agent'];

		$data['rows'] = $this->sotus_reward->get_credit_group();
		$this->load->view('theme/default/header', $data);
		$this->load->view('theme/default/customer_list', $data);
		$this->load->view('theme/default/footer', $data);
	}

	public function invoice()
	{
		$is_rewrite = in_array('mod_rewrite', apache_get_modules());
		$invoice_issue = intval($this->input->get('report_issue'));
		
		$this->load->model('sotus_reward');

		//`pk`, `billing_doc` ,  `billing_date` ,  `credit_group` ,  `c_name`, `sold_to` ,  `party_name` ,  `material_code` ,  `material_label` ,  `itca` ,  `billing_quantity` ,  `billing_su` ,  `net_value` ,  `reward_group` ,  `reward_flag`

		if ($invoice_issue == 0) {
			$data['page_title'] = "Invoice last issue";
			$data['page_desc'] = "Invoice last issue";
			$data['page_keywords'] = "";
		} else {
			
			$data['page_title'] = "Invoice issue ".$invoice_issue;
			$data['page_desc'] = "Invoice last issue ".$invoice_issue;
			$data['page_keywords'] = "";
		}
		$data['device_agent'] = $this->data['device_agent'];


		$data['gen_invoice'] = $this->sotus_reward->get_reward_inv();
		$this->load->view('theme/default/header', $data);
		$this->load->view('theme/default/gen_invoice', $data);
		$this->load->view('theme/default/footer', $data);
	}
	
	public function generate() 
	{
		$this->load->model('sotus_reward');
		/*
		foreach($this->input->post() as $key=>$val) {
			echo $key." => ".$val."<br>";
		}
		*/

		$report_issue = $this->input->post('report_issue');
		$command = $this->input->post('command');

		if (!$report_issue || $report_issue=="") {
			die("Error! Please select your issue");
		}

		if (!$command || $command == "") {
			die("Error! Please choose your genrater");
		}

		if ($this->input->post('command') == 1) {
			$insert_reward_inv = $this->sotus_reward->insert_reward_inv();
			echo "Saved ".$insert_reward_inv."<br>";
			echo anchor('reward/invoice/?report_issue='.$this->input->post('report_issue'), 'Innvoice report', array('title' => 'View your invoice'));
		}

		if ($this->input->post('command') == 2) {
			$this->sotus_reward->insert_reward_val();
			echo anchor('reward/view/?report_issue='.$this->input->post('report_issue'), 'Mailing report', array('title' => 'View your invoice'));
		}
	}

	public function gen() 
	{
		$this->load->model('sotus_reward');		
		$insert_inv = $this->sotus_reward->gen_invoice_reward();
		$insert_value = $this->sotus_reward->insert_reward_value();
		//echo $insert_value;
		redirect($_POST['refresh'], 'refresh');
	}

	public function view() 
	{
		$this->load->model('sotus_reward');
		$sort_id = intval($this->input->get('sort'));
		$gen_reward = $this->sotus_reward->get_reward_value($sort_id);
		$get_mail_text = $this->sotus_reward->mail_text();
		//`sold_to`, `prefix`, `party_name`, `net_value`, `net_a`, `net_b`, `net_coupon`, `net_point`, `co_coupon`, `coupon_a`, `coupon_b`, `point_a`, `point_b`, `region_group`
		
		$data['page_title'] = "Reward generator";
		$data['page_desc'] = "Reward generator";
		$data['page_keywords'] = "";
		$data['gen_reward'] = $gen_reward;
		$data['device_agent'] = $this->data['device_agent'];
		$data['is_rewrite'] = $this->data['is_rewrite'];


		$data['mail_text'] = $get_mail_text;

		$this->load->view('theme/default/header', $data);
		$this->load->view('theme/default/gen_reward', $data);
		$this->load->view('theme/default/footer', $data);
		
	}

	/* Mailing Function 
	* By Komsan
	* Create on 
	* Update 2015-03-26 
	*/

	public function mailing() 
	{
		$this->load->model('sotus_reward');
		$this->sotus_reward->mail_text_insert();
		$thai_month = array ("",
"มกราคม", 
"กุมภาพันธ์", 
"มีนาคม", 
"เมษายน", 
"พฤษภาคม", 
"มิถุนายน", 
"กรกฎาคม", 
"สิงหาคม", 
"กันยายน", 
"ตุลาคม", 
"พฤศจิกายน", 
"ธันวาคม");
		echo '<!doctype html>
<html lang="th">
<head>
<meta charset="UTF-8">
<title>ส่งจดหมาย</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
	font-size: 9pt;
}
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}
</style>
</head>
<body>';
		foreach($this->input->post('pkid') as $key=>$val) {
			$sql = "SELECT * 
			FROM  `reward_value` 
			WHERE  `pk` LIKE  '".$key."'";
			$query = $this->db->query($sql);
			$rs = $query->result();
			foreach ($rs as $row)
			{
				$party_name = $row->prefix ." ".$row->party_name;
				$contact_addr = $row->contact_addr;
				$city = $row->city;
				$province = $row->province;
				$postal_code = $row->postal_code;

				$net_a = $row->net_a;
				$net_b = $row->net_b;

				$net_a_issue = $row->net_a_issue;
				$net_b_issue = $row->net_b_issue;

				$coupon_a = $row->coupon_a;
				$coupon_b = $row->coupon_b;

				
				$coupon_a_issue = $row->coupon_a_issue;
				$coupon_b_issue = $row->coupon_b_issue;
				

				$point_a = $row->point_a;
				$point_b = $row->point_b;
				$report_issue = $row->issue;
				?>
				<?php
			}
			?>
			<div style="page-break-after: always;">
			<table width="100%" border="0">
			<tr>
				<td width="30%" height="75">
				<br>
				<br>
				<br>
				</td>
				<td width="30%">&nbsp;</td>
				<td width="40%">&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="right" style="font-size: 10.5pt;">วันที่ <?php echo date("j") ." ". $thai_month[date("n")] ." พ.ศ. ". (date("Y") + 543); ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
			<td colspan="2" height="185" valign="top" style="font-size: 10.5pt;">
			<div>เรื่อง &nbsp;แจ้งยอดคูปองชิงโชค งานฉลองครบรอบ 20 ปีทอง โซตัส</div>
			<br>
			<div>
			เรียน<br>
				<table width="60%" align="center">
				<tr>
				<td style="font-size: 11.5pt;">
						<?php echo $party_name; ?><br>
						<?php echo $contact_addr; ?><br>
						<?php echo $city; ?>&nbsp;<?php echo $province; ?><br>
						<?php echo $postal_code; ?>
				</td>
				</tr>
				</table>
			</div>
			</td>
			<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3" style="border-bottom: 1px dotted #666;">&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
			<td colspan="3" style="font-size: 10.5pt;">
			<?php 
			if ($this->input->post('header_p1')) {
				?>
				<div style="line-height: 17pt; text-align: justify; text-align-last: justify; text-indent: 90px;"><?php echo nl2br($this->input->post('header_p1')); ?></div><br>
				<?php
			}
			?>
			<?php 
			if ($this->input->post('header_p2')) {
				?>
				<div style="line-height: 17pt; text-align: justify; text-align-last: justify; text-indent: 90px;"><?php echo nl2br($this->input->post('header_p2')); ?></div><br>
				<?php
			}
			?>
			<?php 
			if ($this->input->post('header_p3')) {
				?>
				<div style="line-height: 17pt; text-align: justify; text-align-last: justify; text-indent: 90px;"><?php echo nl2br($this->input->post('header_p3')); ?></div><br>
				<?php
			}
			?>
			</td>
			</tr>
			<tr>
				<td colspan="3">
					<table style="margin-left: 100px; font-size: 10.5pt;" border="1" cellspacing="0" cellpadding="4">
					  <tr>
                      <th rowspan="2">กลุ่มสินค้า</th>
					  <th colspan="2">ยอดซื้อสินค้า (บาท)</th>
					  <th>คูปองจับรางวัลครั้งที่ <?php echo ($report_issue > 7) ? '2':'1'; ?></th>
                      <?php if ($report_issue > 7) { ?>
					  <th rowspan="2">คูปองจับรางวัลครั้งที่ 3</th>
                      <?php } ?>
					  <th rowspan="2">คะแนนสะสม</th>
					  </tr>
                      <tr>					  
					  <th>เดือนนี้</th>
					  <th>สะสม</th>
					  <th>สะสม</th>
					  
                      </tr>
					  <tr>
					  <td style="padding: 4px 22px 4px 4px">สินค้ากลุ่ม A</td>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format($net_a_issue, 0, ".", ","); ?></td>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format($net_a, 0, ".", ","); ?></td>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format($coupon_a, 0, ".", ","); ?></td>
                      <?php if ($report_issue > 7) { ?>
					  <td style="padding: 4px; text-align: right;">&nbsp;</td>
                      <?php } ?>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format($point_a, 0, ".", ","); ?></td>
					  </tr>
					  <tr>
					  <td style="padding: 4px px 4px 4px">สินค้ากลุ่ม B</td>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format($net_b_issue, 0, ".", ","); ?></td>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format($net_b, 0, ".", ","); ?></td>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format($coupon_b, 0, ".", ","); ?></td>
                      <?php if ($report_issue > 7) { ?>
					  <td style="padding: 4px; text-align: right;">&nbsp;</td>
                      <?php } ?>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format($point_b, 0, ".", ","); ?></td>
					  </tr>
					  <tr>
					  <td align="center">รวม</td>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format(($net_a_issue + $net_b_issue), 0, ".", ","); ?></td>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format(($net_a + $net_b), 0, ".", ","); ?></td>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format(($coupon_a + $coupon_b), 0, ".", ","); ?></td>
                      <?php if ($report_issue > 7) { ?>
					  <td style="padding: 4px; text-align: right;">&nbsp;</td>
                      <?php } ?>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format(($point_a + $point_b), 0, ".", ","); ?></td>
					  </tr>
					</table>
				</td>
			</tr>
			<tr>
			<td colspan="3" style="font-size: 10.5pt;">
			<br>
			<?php 
			if ($this->input->post('footer_p1')) {
				?>
				<div style="line-height: 17pt; text-align: justify; text-align-last: justify; text-indent: 90px;"><?php echo nl2br($this->input->post('footer_p1')); ?></div><br>
				<?php
			}
			?>
			<?php 
			if ($this->input->post('footer_p2')) {
				?>
				<div style="line-height: 17pt; text-align: justify; text-align-last: justify; text-indent: 90px;"><?php echo nl2br($this->input->post('footer_p2')); ?></div><br>
				<?php
			}
			?>
			<?php 
			if ($this->input->post('footer_p3')) {
				?>
				<div style="line-height: 17pt; text-align: justify; text-align-last: justify; text-indent: 90px;"><?php echo nl2br($this->input->post('footer_p3')); ?></div><br>
				<?php
			}
			?>
			</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td align="center" nowrap="nowrap" style="font-size: 10.5pt;">
					<p>ขอแสดงความนับถือ<p>
					<p><br></p>
					<p><br></p>
					<p>
								(นายสมพงษ์ สุนทรจิตตานนท์)
					</p>
					<p>
					รองกรรมการผู้จัดการ<br>
					สายงานการตลาดและการขาย
					</p>
				</td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			</tr>
			</table>


				</div>
		<?php
		}
		?>
		</body>
	</html>
	<?php
	}

	/* Old mailing */
	/* */
	function mailing_old() 
	{
		$this->load->model('sotus_reward');
		$this->sotus_reward->mail_text_insert();
		$thai_month = array ("",
"มกราคม", 
"กุมภาพันธ์", 
"มีนาคม", 
"เมษายน", 
"พฤษภาคม", 
"มิถุนายน", 
"กรกฎาคม", 
"สิงหาคม", 
"กันยายน", 
"ตุลาคม", 
"พฤศจิกายน", 
"ธันวาคม");
		echo '<!doctype html>
<html lang="th">
<head>
<meta charset="UTF-8">
<title>ส่งจดหมาย</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
	font-size: 9pt;
}
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}
</style>
</head>
<body>';
		foreach($this->input->post('pkid') as $key=>$val) {
			$sql = "SELECT * 
			FROM  `reward_value` 
			WHERE  `pk` LIKE  '".$key."'";
			$query = $this->db->query($sql);
			$rs = $query->result();
			foreach ($rs as $row)
			{
				$party_name = $row->prefix ." ".$row->party_name;
				$contact_addr = $row->contact_addr;
				$city = $row->city;
				$province = $row->province;
				$postal_code = $row->postal_code;

				$net_a = $row->net_a;
				$net_b = $row->net_b;

				$net_a_issue = $row->net_a_issue;
				$net_b_issue = $row->net_b_issue;

				$coupon_a = $row->coupon_a;
				$coupon_b = $row->coupon_b;

				
				$coupon_a_issue = $row->coupon_a_issue;
				$coupon_b_issue = $row->coupon_b_issue;
				

				$point_a = $row->point_a;
				$point_b = $row->point_b;
				$report_issue = $row->issue;
				?>
				<?php
			}
			?>
			<div style="page-break-after: always;">
			<table width="100%" border="0">
			<tr>
				<td width="30%" height="75">
				<br>
				<br>
				<br>
				</td>
				<td width="30%">&nbsp;</td>
				<td width="40%">&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="right" style="font-size: 10.5pt;">วันที่ <?php echo date("j") ." ". $thai_month[date("n")] ." พ.ศ. ". (date("Y") + 543); ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
			<td colspan="2" height="185" valign="top" style="font-size: 10.5pt;">
			<div>เรื่อง &nbsp;แจ้งยอดคูปองชิงโชค งานฉลองครบรอบ 20 ปีทอง โซตัส</div>
			<br>
			<div>
			เรียน<br>
				<table width="60%" align="center">
				<tr>
				<td style="font-size: 11.5pt;">
						<?php echo $party_name; ?><br>
						<?php echo $contact_addr; ?><br>
						<?php echo $city; ?>&nbsp;<?php echo $province; ?><br>
						<?php echo $postal_code; ?>
				</td>
				</tr>
				</table>
			</div>
			</td>
			<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3" style="border-bottom: 1px dotted #666;">&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
			<td colspan="3" style="font-size: 10.5pt;">
			<?php 
			if ($this->input->post('header_p1')) {
				?>
				<div style="line-height: 17pt; text-align: justify; text-align-last: justify; text-indent: 90px;"><?php echo nl2br($this->input->post('header_p1')); ?></div><br>
				<?php
			}
			?>
			<?php 
			if ($this->input->post('header_p2')) {
				?>
				<div style="line-height: 17pt; text-align: justify; text-align-last: justify; text-indent: 90px;"><?php echo nl2br($this->input->post('header_p2')); ?></div><br>
				<?php
			}
			?>
			<?php 
			if ($this->input->post('header_p3')) {
				?>
				<div style="line-height: 17pt; text-align: justify; text-align-last: justify; text-indent: 90px;"><?php echo nl2br($this->input->post('header_p3')); ?></div><br>
				<?php
			}
			?>
			</td>
			</tr>
			<tr>
				<td colspan="3">
					<table style="margin-left: 100px; font-size: 10.5pt;" border="1" cellspacing="0" cellpadding="4">
					  <tr>
                      <th rowspan="2">กลุ่มสินค้า</th>
					  <th colspan="2">ยอดซื้อสินค้า (บาท)</th>
					  <th colspan="2">คูปองจับรางวัลครั้งที่ <?php echo ($report_issue > 7) ? '2':'1'; ?></th>
                      <?php if ($report_issue > 7) { ?>
					  <th rowspan="2">คูปองจับรางวัลครั้งที่ 3</th>
                      <?php } ?>
					  <th rowspan="2">คะแนนสะสม</th>
					  </tr>
                      <tr>					  
					  <th>เดือนนี้</th>
					  <th>สะสม</th>
					  <th>เดือนนี้</th>
					  <th>สะสม</th>
                      </tr>
					  <tr>
					  <td style="padding: 4px 22px 4px 4px">สินค้ากลุ่ม A</td>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format($net_a_issue, 0, ".", ","); ?></td>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format($net_a, 0, ".", ","); ?></td>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format($coupon_a_issue, 0, ".", ","); ?></td>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format($coupon_a, 0, ".", ","); ?></td>
                      <?php if ($report_issue > 7) { ?>
					  <td style="padding: 4px; text-align: right;">&nbsp;</td>
                      <?php } ?>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format($point_a, 0, ".", ","); ?></td>
					  </tr>
					  <tr>
					  <td style="padding: 4px px 4px 4px">สินค้ากลุ่ม B</td>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format($net_b_issue, 0, ".", ","); ?></td>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format($net_b, 0, ".", ","); ?></td>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format($coupon_b_issue, 0, ".", ","); ?></td>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format($coupon_b, 0, ".", ","); ?></td>
                      <?php if ($report_issue > 7) { ?>
					  <td style="padding: 4px; text-align: right;">&nbsp;</td>
                      <?php } ?>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format($point_b, 0, ".", ","); ?></td>
					  </tr>
					  <tr>
					  <td align="center">รวม</td>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format(($net_a_issue + $net_b_issue), 0, ".", ","); ?></td>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format(($net_a + $net_b), 0, ".", ","); ?></td>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format(($coupon_a_issue + $coupon_b_issue), 0, ".", ","); ?></td>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format(($coupon_a + $coupon_b), 0, ".", ","); ?></td>
                      <?php if ($report_issue > 7) { ?>
					  <td style="padding: 4px; text-align: right;">&nbsp;</td>
                      <?php } ?>
					  <td style="padding: 4px; text-align: right;"><?php echo number_format(($point_a + $point_b), 0, ".", ","); ?></td>
					  </tr>
					</table>
				</td>
			</tr>
			<tr>
			<td colspan="3" style="font-size: 10.5pt;">
			<br>
			<?php 
			if ($this->input->post('footer_p1')) {
				?>
				<div style="line-height: 17pt; text-align: justify; text-align-last: justify; text-indent: 90px;"><?php echo nl2br($this->input->post('footer_p1')); ?></div><br>
				<?php
			}
			?>
			<?php 
			if ($this->input->post('footer_p2')) {
				?>
				<div style="line-height: 17pt; text-align: justify; text-align-last: justify; text-indent: 90px;"><?php echo nl2br($this->input->post('footer_p2')); ?></div><br>
				<?php
			}
			?>
			<?php 
			if ($this->input->post('footer_p3')) {
				?>
				<div style="line-height: 17pt; text-align: justify; text-align-last: justify; text-indent: 90px;"><?php echo nl2br($this->input->post('footer_p3')); ?></div><br>
				<?php
			}
			?>
			</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td align="center" nowrap="nowrap" style="font-size: 10.5pt;">
					<p>ขอแสดงความนับถือ<p>
					<p><br></p>
					<p><br></p>
					<p>
								(นายสมพงษ์ สุนทรจิตตานนท์)
					</p>
					<p>
					รองกรรมการผู้จัดการ<br>
					สายงานการตลาดและการขาย
					</p>
				</td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			</tr>
			</table>


				</div>
		<?php
		}
		?>
		</body>
	</html>
	<?php
	}

	public function material() {
		$this->load->model('sotus_reward');

		$data['page_title'] = "Material";
		$data['page_desc'] = "Sotus Material";
		$data['page_keywords'] = "";
		$data['device_agent'] = $this->data['device_agent'];

		$data['fields'] = $this->sotus_reward->get_material_fields();
		$data['rows'] = $this->sotus_reward->get_material();
		$this->load->view('theme/default/header', $data);
		$this->load->view('theme/default/get_material', $data);
		$this->load->view('theme/default/footer', $data);
	}

}

/* End of file reward.php */
/* Location: ./application/controllers/reward.php */