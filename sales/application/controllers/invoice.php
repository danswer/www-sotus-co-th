<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invoice extends CI_Controller {

	/**
	 * Reward Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/invoice
	 *	- or -  
	 * 		http://example.com/index.php/invoice/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/invoice/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function __construct()
	{
		parent::__construct();
		if ($this->agent->is_mobile('iphone'))
		{
			$this->data['device_agent'] = $this->agent->mobile();
		}
		else if ($this->agent->is_mobile())
		{
			$this->data['device_agent'] = $this->agent->mobile();
		}
		else
		{
			$this->data['device_agent'] = $this->agent->browser().' '.$this->agent->version();
		}

		$this->data['is_rewrite'] = in_array('mod_rewrite', apache_get_modules());
		$this->load->library(array(
			//'database', 
			//'session', 
			//'user_agent',
			'parser',
			'calendar',
		));
		$this->load->helper(array(
			'form',
		));
		$this->config->load('reward/config', TRUE);

	}

	public function index()
	{
		$data = array();

		$this->load->model('sotus_invoice');
		$cust_list = $this->sotus_invoice->customers();


		$data['device_agent'] = $this->data['device_agent'];
		$rs = $this->sotus_invoice->document_list();
		$data['rs'] = $rs;

		$data['meta_title'] = "Sotus Invoice";
		$data['meta_keyword'] = "Sotus Invoice";
		$data['meta_description'] = "Sotus Invoice";

		$this->load->view('theme/invoice/header', $data);
		$this->load->view('theme/default/inv_dashboard', $data);
		$this->load->view('theme/invoice/footer', $data);
	}
	public function view()
	{
		$data = array();
		$this->load->model('sotus_invoice');
		$rs = $this->sotus_invoice->get_credit_group();
		foreach ($rs as $field)
		{
			$sql = "SELECT
			invoice.billing_doc,
			invoice.billing_date,
			customer.credit_group,
			customer.customer,
			customer.prefix_name,
			customer.customer_name,
			material.reward_group,
			invoice.material_code,
			invoice.material_label,
			invoice.itca,
			invoice.billing_quantity,
			invoice.billing_su,
			invoice.net_value,
			SUM(net_value) AS net_value
			FROM
			invoice
			INNER JOIN material ON invoice.material_code = material.material
			INNER JOIN customer ON invoice.sold_to = customer.customer
			WHERE
			invoice.billing_date BETWEEN '2014-12-01' AND '2014-12-31' AND
			invoice.sold_to = customer.customer AND
			customer.credit_group = ".$field->credit_group."
			GROUP BY
			customer.credit_group";
			$query = $this->db->query($sql);
			return $query->result();
		}
		

		$data['page_title'] = "Sotus Invoice";
	}
}
?>